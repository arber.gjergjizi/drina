package at.co.service;

import at.co.dto.DynamicOdata;
import at.co.header.DynamicsHeaderFactory;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@RegisterRestClient
@RegisterClientHeaders(DynamicsHeaderFactory.class)
public interface DynamicsService {
    @GET
    DynamicOdata getOdata();
}
