package at.co.service;

import at.co.dto.DynamicAccount;
import at.co.entity.*;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.gradle.internal.impldep.org.apache.commons.lang.StringUtils.substring;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@ApplicationScoped
public class CompanyService {

    @Inject
    @RestClient
    DynamicsService dynamicsService;

    private List<ExternalOrganisationType> externalOrganisationTypes;
    private List<Place> places;
    private List<Country> countries;

    public void setCompanies() {
        Field[] fields = DynamicAccount.class.getFields();
        Pattern pattern = Pattern.compile("([a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)|([0-9]*\\\"*\\.*\\/*-*\\&*\\_*\\#*\\,*\\–*\\(*\\)*\\'*\\+*\\|*\\**\\[*\\]*\\{*\\}*\\:*\\;*\\$*\\€*\\\\*\\!*[\\u00C0-\\u017Fa-zA-Z]*\\u0020*[0-9]*)*|\\+*(\\s*[0-9]*-*\\.*)*" , Pattern.UNICODE_CASE);

        this.externalOrganisationTypes = ExternalOrganisationType.listAll();
        this.places = Place.listAll();
        this.countries = Country.listAll();

        List<DynamicAccount> newDynamicAccounts = this.dynamicsService.getOdata().value.parallelStream()
                .filter(o -> o.statecode == 0 && o.accountid != null && o.kcrm_branche  != null && o.address1_country != null)
                .filter(o -> this.externalOrganisationTypes.parallelStream().anyMatch(i -> i.type.equals(o.kcrm_branche)))
                .filter(o -> this.countries.parallelStream().anyMatch(i -> i.name.equals(o.address1_country)))
                .filter(o -> Arrays.stream(fields).map(i -> getValue(o, i)).filter(Objects::nonNull)
                        .allMatch(i -> pattern.matcher(i.toString()).matches()))
                .collect(Collectors.toList());

        List<ExternalOrganisation> externalOrganisations = ExternalOrganisation.listAll();
        newDynamicAccounts.parallelStream().forEach(o -> processExternalOrganisation(o, externalOrganisations));
    }

    private Object getValue(DynamicAccount o, Field i) {
        try {
            return i.get(o);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void processExternalOrganisation(DynamicAccount dynamicAccount, List<ExternalOrganisation> externalOrganisations) {
        Optional<ExternalOrganisation> externalOrganisationOptional =
                externalOrganisations.stream()
                        .filter(o -> o.sourceId != null && o.sourceId.equals(dynamicAccount.accountid)).findFirst();

        this.mapAccountToExternalOrganisation(dynamicAccount, externalOrganisationOptional.orElseGet(ExternalOrganisation::new));
    }

    @Transactional
    void mapAccountToExternalOrganisation(DynamicAccount dynamicAccount, ExternalOrganisation source) {
        ExternalOrganisation externalOrganisation = source.id == null ? source : ExternalOrganisation.findById(source.id);
        externalOrganisation.sourceId = dynamicAccount.accountid;
        externalOrganisation.name = substring(dynamicAccount.name, 0, 199);
        externalOrganisation.externalOrganisationType =
                this.externalOrganisationTypes.stream().filter(o -> o.type.equals(dynamicAccount.kcrm_branche))
                        .findFirst().orElseThrow();

        externalOrganisation.organisationId = new BigDecimal(BigInteger.ONE);
        externalOrganisation.createdDate = new Date(System.currentTimeMillis());
        externalOrganisation.address = getAddress(dynamicAccount,
                externalOrganisation.id != null ? Address.findById(externalOrganisation.address.id) : new Address());
        this.saveOrUpdate(externalOrganisation);

    }

    @Transactional
    Address getAddress(DynamicAccount dynamicAccount, Address address) {
        address.createdDate = new Date(System.currentTimeMillis());
        address.street = substring(dynamicAccount.address1_line1, 0, 99);
        address.websiteUrl = substring(dynamicAccount.websiteurl, 0, 99);
        address.email = substring(dynamicAccount.emailaddress1, 0, 99);
        address.telephone = substring(dynamicAccount.telephone1, 0, 255);
        address.place = this.places.stream().filter(o -> o.code.equals(dynamicAccount.address1_postalcode)
                && o.place.equals(dynamicAccount.address1_city)).findFirst().orElse(null);
        address.foreignPlace = address.place == null ? dynamicAccount.address1_city : null;
        address.foreignCode = address.place == null ? dynamicAccount.address1_postalcode : null;
        address.country = this.countries.stream().filter(o -> o.name.equals(dynamicAccount.address1_country))
                .findFirst().orElseThrow();
        this.saveOrUpdate(address);
        return address;
    }

    private <T extends HelaAbstractEntity> void saveOrUpdate(T entity) {
        if (entity.id == null) {
            entity.persistAndFlush();
        }
    }
}
