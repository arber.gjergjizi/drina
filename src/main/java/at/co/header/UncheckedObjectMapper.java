package at.co.header;

import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletionException;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
class UncheckedObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {
    Map<String,String> readValue(String content) {
        try {
            return this.readValue(content, new TypeReference<>(){});
        } catch (IOException ioe) {
            throw new CompletionException(ioe);
        }
    }
}
