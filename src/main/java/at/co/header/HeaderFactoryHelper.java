package at.co.header;

import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
public class HeaderFactoryHelper {
    public MultivaluedMap<String, String> getMultivaluedMap(@NotNull String url, @NotNull String data) throws IOException, InterruptedException {
        UncheckedObjectMapper objectMapper = new UncheckedObjectMapper();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .timeout(Duration.ofMinutes(2))
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .POST(HttpRequest.BodyPublishers.ofString(data))
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        MultivaluedMap<String, String> result = new MultivaluedMapImpl<>();
        result.add("Authorization", "Bearer " + objectMapper.readValue(response.body()).get("access_token"));
        return result;
    }
}
