package at.co.header;

import lombok.SneakyThrows;
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.MultivaluedMap;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@RequestScoped
public class DynamicsHeaderFactory implements ClientHeadersFactory {

    @SneakyThrows
    @Override
    public MultivaluedMap<String, String> update(MultivaluedMap<String, String> incomingHeaders, MultivaluedMap<String, String> clientOutgoingHeaders) {
        String data = "payload";
        String api = "https://login.microsoftonline.com/<ID>/oauth2/v2.0/token";
        HeaderFactoryHelper headerFactoryHelper = new HeaderFactoryHelper();
        return headerFactoryHelper.getMultivaluedMap(api, data);
    }
}
