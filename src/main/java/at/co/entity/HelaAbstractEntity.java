package at.co.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@MappedSuperclass
public abstract class HelaAbstractEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue
    @Column(name = "NR", nullable = false)
    public BigDecimal id;

    public HelaAbstractEntity() {
    }
}
