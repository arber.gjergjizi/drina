package at.co.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@Entity
@Table(name = "ORGANISATIONEN")
public class ExternalOrganisation extends HelaAbstractEntity {

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ADR_NR")
    public Address address;

    @ManyToOne()
    @JoinColumn(name="EXT_ORGTYP_NR")
    public ExternalOrganisationType externalOrganisationType;

    @Column(name = "name", nullable = false)
    public String name;

    @Column(name = "ERSTELLT_AM", nullable = false)
    public Date createdDate;

    @Column(name = "ERSTELLT_VON", nullable = false)
    public String createdFrom;

    @Column(name = "ORG_NR", nullable = false)
    public BigDecimal organisationId;

    @Column(name = "QUELLEN_BRANCHE")
    public String sourceId;
}
