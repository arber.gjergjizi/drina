package at.co.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@Entity
@Immutable
@Table(name = "POSTLEITZAHLEN")
public class Place extends PanacheEntityBase {
    @Id
    @GeneratedValue
    @Column(name = "NR")
    public BigDecimal id;

    @Column(name = "POSTLEITZAHL")
    public String code;

    @Column(name = "ORT")
    public String place;
}
