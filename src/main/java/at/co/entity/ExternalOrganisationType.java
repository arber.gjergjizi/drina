package at.co.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@Entity
@Immutable
@Table(name = "TYPEN")
public class ExternalOrganisationType extends PanacheEntityBase {
    @Id
    @GeneratedValue
    @Column(name = "NR", nullable = false)
    public BigDecimal id;

    @Column(name = "KURZBEZEICHNUNG", nullable = false)
    public String type;

    public String name;

}