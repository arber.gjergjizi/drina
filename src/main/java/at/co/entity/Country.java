package at.co.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@Entity
@Immutable
@Table(name = "LAENDER")
public class Country extends PanacheEntityBase {
    @Id
    @GeneratedValue
    @Column(name = "NR")
    public BigDecimal id;

    @Column(name = "KURZBEZEICHNUNG")
    public String shortName;

    @Column(name = "NAME")
    public String name;
}