package at.co.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@Entity
@Table(name = "ADRESSEN")
public class Address extends HelaAbstractEntity  {

    @Column(name = "ERSTELLT_AM", nullable = false)
    public Date createdDate;

    @Column(name = "ERSTELLT_VON", nullable = false)
    public String createdFrom;

    @Column(name = "STRASSE_HAUSNUMMER", nullable = false)
    public String street;

    @Column(name = "WWW_HOMEPAGE")
    public String websiteUrl;

    @Column(name = "EMAIL_ADRESSE")
    public String email;

    @Column(name = "TELEFON_NUMMER")
    public String telephone;

    @Column(name = "ORT_AUSLAND")
    public String foreignPlace;

    @Column(name = "POSTLEITZAHL_AUSLAND")
    public String foreignCode;

    @ManyToOne()
    @JoinColumn(name="PLZ_NR")
    public Place place;

    @ManyToOne()
    @JoinColumn(name="LAND_NR")
    public Country country;
}
