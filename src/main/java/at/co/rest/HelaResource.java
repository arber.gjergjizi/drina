package at.co.rest;

import at.co.service.CompanyService;
import org.jboss.resteasy.spi.HttpRequest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/api")
@ApplicationScoped
public class HelaResource {


    @Inject
    CompanyService companyService;

    @POST
    @Path("/company")
    public Response setCompany(@Context HttpRequest req) {
        companyService.setCompanies();
        return Response.ok().build();
    }
}