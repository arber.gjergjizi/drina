package at.co.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DynamicAccount {
    public Integer statecode;
    public String name;
    public String accountid;
    public String websiteurl;
    public String emailaddress1;
    public String address1_city;
    public String address1_postalcode;
    public String address1_country;
    public String address1_line1;
    public String telephone1;
    public String kcrm_branche;
}
