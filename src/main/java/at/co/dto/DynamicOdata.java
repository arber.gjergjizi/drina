package at.co.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * @author Arbër Gjergjizi <arber.gjergjizi@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DynamicOdata {
    public List<DynamicAccount> value;
}
